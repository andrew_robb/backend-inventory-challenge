import { snakeCase } from 'lodash';
import {
  WMSWarehouseMeta,
  inventoryUpdate,
  RecordWithWMS,
  SkuBatchData,
  SkuBatchToSkuId,
  skuBatchUpdate,
  InventoryAggregateBody,
} from './interfaces.util';
import {
    appData,
    appSkuBatchData, appSkuBatchDataForSkuBatchIds,
    skuBatchIdsFromInventoryDb,
    skuBatchIdsFromAppDb,
    warehouseData
} from "./db/data";
import {
    getUpdateForSkuBatchRecord,
    formatSqlValue,
} from "./db/sql.util";
import { createInventory, createInventoryAggregate, updateInventory, updateInventoryAggregate } from './services/inventoryService';

const logger = console;

/**
 * Create a list of records for a skuBatch record that maps skuBatchId + warehouseId
 * @param skuBatchRecord
 */
const makeWarehouseRecordsForSkuBatchRecord = (skuBatchRecord: SkuBatchToSkuId): RecordWithWMS[] => {
  return warehouseData.map(
    (warehouse: WMSWarehouseMeta): RecordWithWMS => ({
        skuBatchId: skuBatchRecord.skuBatchId,
        skuId: skuBatchRecord.skuId,
        wmsId: skuBatchRecord.wmsId,
        quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure ?? 1,
        isArchived: skuBatchRecord.isArchived,
        isDeleted: skuBatchRecord.isDeleted,
        warehouseId: warehouse.warehouseId,
      }),
  );
};

/**
 * Converts a list of skuBatchIds from the app db into an insert to inventory.
 * @param skuBatchIdsToInsert
 */
export async function skuBatchToInserts(skuBatchIdsToInsert: string[]): Promise<string[]> {
  const badSkuBatchCounter = { count: 0 };

  // create our inserts
  const inserts: Promise<string | undefined> [] = skuBatchIdsToInsert
    .reduce((arr: RecordWithWMS[], skuBatchId: string): RecordWithWMS[] => {
      const skuBatchRecordFromAppDb: SkuBatchToSkuId | undefined = appData.find(
        (skuBatchToSkuId: SkuBatchToSkuId): boolean => skuBatchToSkuId.skuBatchId === skuBatchId,
      );

      if (!skuBatchRecordFromAppDb) {
        logger.error(`no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`);
        badSkuBatchCounter.count += 1;
        return arr;
      }

      arr.push(...makeWarehouseRecordsForSkuBatchRecord(skuBatchRecordFromAppDb));
      return arr;
    }, [])
    .map(async (record: RecordWithWMS) => {
      try {
        await createInventory(record)
        await createInventoryAggregate(record)
      } catch (error) {
        logger.error(`Error in inserting sku batch (sku: ${record.skuId}, batch: ${record.skuBatchId}):`, error)
      }
      return `send request (col_1, col_2) values (${record.skuId}, ${record.skuBatchId})`
    });
  const insertResults = await Promise.all(inserts)
  
  logger.log(`created inserts [count=${insertResults?.length}, badSkuBatchRecordCount=${badSkuBatchCounter.count}]`);

  return insertResults.filter((r): r is string => r !== undefined) ?? [];
}

/**
 * Diffs the inventory between app SkuBatch and inventory to determine
 * what we need to copy over.
 */
export async function getDeltas(): Promise<string[]> {
  try {
    const inventorySkuBatchIds: Set<string> = new Set<string>(skuBatchIdsFromInventoryDb
        .map((r: { skuBatchId: string }) => r.skuBatchId));
    return [...new Set<string>(skuBatchIdsFromAppDb.map((r: { id: string }) => r.id))]
        .filter((x: string) => !inventorySkuBatchIds.has(x));
  } catch (err) {
    logger.error('error querying databases for skuBatchIds');
    logger.error(err);
    throw err;
  }
}

/**
 * Builds list of SQL updates - this is a pretty simple function to turn a delta
 * into a SQL update
 * @param delta
 */
export const makeUpdates = (delta: skuBatchUpdate): string[] => {
    // convert updates to sql and push updates
  const updatesToMake = delta.updates
    .map((ud: inventoryUpdate) => `${snakeCase(ud.field)} = ${formatSqlValue(ud.newValue)}`)
    .join('; ');

  return [
    getUpdateForSkuBatchRecord('inventory', updatesToMake, delta.skuBatchId),
    getUpdateForSkuBatchRecord('inventory_aggregate', updatesToMake, delta.skuBatchId),
  ];
};

/**
 * Finds the deltas between two lists of SkuBatchData
 * @param appSkuBatchData
 * @param inventorySkuBatchData
 */
export const findDeltas = (
    appSkuBatchData: SkuBatchData[],
    inventorySkuBatchData: SkuBatchData[],
): skuBatchUpdate[] => {
  logger.log('finding data changes between inventory and app SkuBatch datasets');

  return appSkuBatchData
    .map((appSbd: SkuBatchData) => {
      const inventoryRecord: SkuBatchData | undefined = inventorySkuBatchData
          .find((r: SkuBatchData): boolean => r.skuBatchId == appSbd.skuBatchId);

      if (!inventoryRecord) {
        // if we cannot find the matching record, we have a problem
        logger.warn(`cannot find matching inventory record! [skuBatchId=${appSbd.skuBatchId}]`);
        // instead of throwing an error, return empty update array which will
        // get filtered out at the end of this chain
        return { skuBatchId: '', updates: [] };
      }

      // go through each key and see if it is different, if so, track it
      const updates: inventoryUpdate[] = Object.keys(inventoryRecord)
        .filter((k: string) => !['skuBatchId'].includes(k))
        .reduce((recordUpdates: inventoryUpdate[], key: string): inventoryUpdate[] => {
          const inventoryValue = inventoryRecord[key as keyof typeof inventoryRecord];
          const appValue = appSbd[key as keyof typeof appSbd];

          if (key == 'skuId' && inventoryValue != null) {
            // if the key is skuId and the current value is set, we won't update
            return recordUpdates;
          }

          if (inventoryValue != appValue) {
            recordUpdates.push({ field: key, newValue: appValue });
          }

          return recordUpdates;
        }, [] as inventoryUpdate[]);

      return {
        skuBatchId: inventoryRecord.skuBatchId,
        updates,
      };
    })
    .filter((sbu: skuBatchUpdate) => sbu.updates.length != 0);
};

/**
 * Finds changes in data between the app SkuBatch+Sku and inventory tables
 */
export async function findChangesBetweenDatasets(): Promise<skuBatchUpdate[]> {
  logger.log('finding app SkuBatch data that has changed and <> the inventory data');

  const updates: skuBatchUpdate[] = await [appSkuBatchData].reduce(
    async (accumPromise: Promise<skuBatchUpdate[]>, inventorySkuBatchData: SkuBatchData[]) => {
      const accum: skuBatchUpdate[] = await accumPromise;
      const skuBatchIds: string[] = inventorySkuBatchData.map((sbd: SkuBatchData) => sbd.skuBatchId);

      logger.log(`querying Logistics.SkuBatch for data [skuBatchIdCount=${skuBatchIds.length}]`);
      // fetch SkuBatch+Sku data from the app database
      const appSkuBatchData: SkuBatchData[] = appSkuBatchDataForSkuBatchIds;

      // if we have a count mismatch, something is wrong, and we should log out a warning
      if (appSkuBatchData.length != inventorySkuBatchData.length) {
        // implement the logic to log a message with the IDs missing from app
        // data that exist in the inventory data
        const appSkuBatchIdArr = appSkuBatchData.map((r: { skuBatchId: string }) => r.skuBatchId)
        const appSkuBatchDataIds: Set<string> = new Set<string>(appSkuBatchIdArr);
        const missingAppValues = skuBatchIds
            .filter((x: string) => !appSkuBatchDataIds.has(x));
        if(missingAppValues.length > 0){
          logger.warn(`The following IDs are missing from the app data that exist in the inventory data ${missingAppValues.toString()}`);
        } else {
          /*    
            // This would allow us to get the IDs missing from inventory
            // data that exist in the app data
            const inventorySkuBatchIds: Set<string> = new Set<string>(skuBatchIds);
            const missingInventoryValues [...new Set<string>(appSkuBatchIdArr)]
              .filter((x: string) => !inventorySkuBatchIds.has(x));
            
            logger.warn(`The following IDs are missing from the inventory data that exist in the app data ${missingAppValues.toString()}`);
          */
            logger.warn(`There are IDs that are in the app data but not the inventory data`);
        }
        /*    
          // This would allow us to get the IDs in either the app or inventory data
          // not in the other data array
          const missingValues = skuBatchIds.filter(x => !appSkuBatchIdArr.includes(x))
                        .concat(appSkuBatchIdArr.filter(x => !skuBatchIds.includes(x)));
          logger.warn(`The following IDs are not shared between datasets ${missingValues.toString()}`);
        */
      }

      // push our new sql updates into the accumulator list
      const ds: skuBatchUpdate[] = findDeltas(appSkuBatchData, inventorySkuBatchData);
      accum.push(...ds);
      return accum;
    },
    Promise.resolve([] as skuBatchUpdate[]),
  );

  logger.log(`built updates [count=${updates.length}]`);

  return updates;
}

/**
 * Updates inventory data from app SkuBatch and Sku
 */
export async function copyMissingInventoryRecordsFromSkuBatch(): Promise<void | Error> {
  logger.log('copying missing inventory records from app Sku/SkuBatch');

  // find out what skuBatchIds don't exist in inventory
  const skuBatchIdsToInsert: string[] = await getDeltas();
  logger.log(`copying new skuBatch records... [skuBatchCount=${skuBatchIdsToInsert.length}]`);
  try {
    const inserts = await skuBatchToInserts(skuBatchIdsToInsert);

    inserts.map(async (batchId) => {
      const newInventoryRecord = appData.find((r: SkuBatchToSkuId): boolean => r.skuBatchId == batchId)
      if(!newInventoryRecord){
        logger.error('Unable to find app item by batch ID:', batchId)
      } else {
        const wareHouseRecords = makeWarehouseRecordsForSkuBatchRecord(newInventoryRecord)
        wareHouseRecords.map(async (record: RecordWithWMS) => {
          await createInventory(record)
        })
        await createInventoryAggregate(newInventoryRecord)
      }
    })
  } catch (err) {
    console.error('Error in copying missing inventory:', err)
    throw err;
  }

  logger.log('done updating additive data to inventory from app db');
}

/**
 * Pulls inventory and SkuBatch data and finds changes in SkuBatch data
 * that are not in the inventory data.
 */
export async function updateInventoryDeltasFromSkuBatch(): Promise<void> {
  logger.log('updating inventory from deltas in "SkuBatch" data');

  try {
    const sqlUpdates: skuBatchUpdate[] = await findChangesBetweenDatasets();

    sqlUpdates.map(async (batch: skuBatchUpdate) => {
      const batchId = batch.skuBatchId
      const previousValue = appData.find((r: SkuBatchToSkuId): boolean => r.skuBatchId == batchId)
      let updatedBatch = {
        ...previousValue,
        skuBatchId: batchId
      }
      batch.updates.map((update) => {
        updatedBatch = {
          ...previousValue,
          [update.field]: update.newValue,
          skuBatchId: batchId
        }
      })
      const wareHouseRecords = makeWarehouseRecordsForSkuBatchRecord(updatedBatch as SkuBatchToSkuId)
      wareHouseRecords.map(async (record: RecordWithWMS) => {
        await updateInventory(record)
      })
      await updateInventoryAggregate(updatedBatch as InventoryAggregateBody)
    })
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating inventory from deltas from app db');
}

/**
 * Primary entry point to sync SkuBatch data from the app
 * database over to the inventory database
 */
export async function sync(): Promise<void | Error> {
  try {
    await copyMissingInventoryRecordsFromSkuBatch();
    await updateInventoryDeltasFromSkuBatch();
  } catch (err) {
    logger.error('error syncing skuBatch data');
    return Promise.reject(err);
  }
}