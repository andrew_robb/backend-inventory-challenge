import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import { 
    InventoryAggregateBody, 
    InventoryAggregateResponse, 
    InventoryBody, 
    InventoryResponse 
} from 'src/interfaces.util';

const nabisUrl = 'https://local-inventory.nabis.dev/v1/'

const sendRequest = async <T>(config: AxiosRequestConfig): Promise<T> => {
    try {
        // Send Request and get response of type defined by function call
        const response: AxiosResponse<T> = await axios(config);
        // Return data response
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            // Axios error
            const axiosError: AxiosError = error;
            if (axiosError.response) {
                // Server responded with a non-2xx status code
                console.error('Request failed with status code:', axiosError.response.status);
                console.error('Response data:', axiosError.response.data);
            } else if (axiosError.request) {
                // No response received
                console.error('No response received:', axiosError.request);
            } else {
                // Something else happened while setting up the request
                console.error('Error setting up the request:', axiosError.message);
            }
        } else {
            // Non-Axios error
            console.error('Non-Axios error occurred:', error);
        }
        // Re-throw the error to be handled by the caller
        throw error;
    }
}

export const createInventory = async (data: InventoryBody) => {
    try {
        // Set up config value for creating inventory data
        const config = {
            method: 'POST',
            url: `${nabisUrl}/inventory`,
            body: data
        }
        await sendRequest<InventoryResponse>(config)
    } catch(error) {
        console.error('Unable to create inventory:', error)
        throw error
    }
}

export const updateInventory = async (data: InventoryBody) => {
    try {
        // Set up config value for updating inventory data
        const config = {
            method: 'PUT',
            url: `${nabisUrl}/inventory`,
            body: data
        }
        await sendRequest<InventoryResponse>(config)
    } catch(error) {
        console.error('Unable to updating inventory:', error)
        throw error
    }
}

export const createInventoryAggregate = async (data: InventoryAggregateBody) => {
    try {
        // Set up config value for creating inventory aggregate data
        const config = {
            method: 'POST',
            url: `${nabisUrl}/inventory-aggregate`,
            body: data
        }
        await sendRequest<InventoryAggregateResponse>(config)
    } catch(error) {
        console.error('Unable to create inventory aggregate:', error)
        throw error
    }
}

export const updateInventoryAggregate =  async (data: InventoryAggregateBody) => {
    try {
        // Set up config value for creating inventory aggregate data
        const config = {
            method: 'PUT',
            url: `${nabisUrl}/inventory-aggregate`,
            body: data
        }
        await sendRequest<InventoryAggregateResponse>(config)
    } catch(error) {
        console.error('Unable to update inventory aggregate:', error)
        throw error
    }
}